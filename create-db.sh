#!/bin/bash

DB_HOST=${DB_HOST:=""}
DB_ADMIN_USERNAME=${DB_ADMIN_USERNAME:=""}
DB_NAME=${DB_NAME:=""}
DB_USERNAME=${DB_USERNAME:=""}

{
    if [ -z "$DB_HOST" ]; then  
        echo -n "host: "
        read DB_HOST
    fi

    if [ -z "$DB_ADMIN_USERNAME" ]; then 
        echo -n "admin username: "
        read DB_ADMIN_USERNAME
    fi

    echo -n "admin password: "
    read -s DB_ADMIN_PASSWORD
    echo

    if [ -z "$DB_NAME" ]; then 
        echo -n "database name: "
        read DB_NAME
    fi

    if [ -z "$DB_USERNAME" ]; then 
        echo -n "username: "
        read DB_USERNAME
    fi

    echo -n "password: "
    read -s DB_PASSWORD
    echo

    echo $DB_HOST > DB_HOST
    echo $DB_ADMIN_USERNAME > DB_ADMIN_USERNAME
    echo $DB_ADMIN_PASSWORD > DB_ADMIN_PASSWORD
    echo $DB_NAME > DB_NAME
    echo $DB_USERNAME > DB_USERNAME
    echo $DB_PASSWORD > DB_PASSWORD

    kubectl create namespace db-creation
    kubectl create secret generic --namespace db-creation db-creation-secret \
        --from-file=./DB_HOST \
        --from-file=./DB_ADMIN_USERNAME \
        --from-file=./DB_ADMIN_PASSWORD \
        --from-file=./DB_NAME \
        --from-file=./DB_USERNAME \
        --from-file=./DB_PASSWORD
    kubectl apply -f https://bitbucket.org/cloud-logibec/postgres-db-creation/raw/HEAD/job.yml
    echo "wait 60 seconds before deleting the namespace, do not close ..."
    sleep 60
    
}

rm DB_*
kubectl delete namespace db-creation