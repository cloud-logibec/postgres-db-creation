# Create Azure Postgres database 
---

This repository is intented for Azure Postgres database creation. The postgres instance is only available from the cluster so 
by executing this script, you will be able to create a new database and a user inside the postgres database from the kubernetes cluster.

bash <(curl -s https://bitbucket.org/cloud-logibec/postgres-db-creation/raw/HEAD/create-db.sh)